import axios from "axios";
import refreshToken from "./refreshToken";

const axiosInstance = axios.create();

axiosInstance.interceptors.response.use(value => {
	return value;
}, async (error) => {
	let originalRequest = error.config;

	if (error.response.status === 401 && !originalRequest._retry) {
		await refreshToken();
		originalRequest._retry = true;
		return axiosInstance(originalRequest);
	}

	return Promise.reject(error);
});

export default axiosInstance;
