import axios from "../axiosWithRefreshToken";
import wrapAxiosRequest from "../wrapAxiosRequest";

const deleteExperience = async (experienceId) => {
	return wrapAxiosRequest(axios.delete(`/api/experience/${experienceId}`));
};

export default deleteExperience;
