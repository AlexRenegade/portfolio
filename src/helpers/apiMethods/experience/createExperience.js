import axios from "../axiosWithRefreshToken";
import wrapAxiosRequest from "../wrapAxiosRequest";

const createExperience = async (title, description, company, url, iconUrl, startDate, endDate) => {
	return wrapAxiosRequest(axios.post("/api/experience", {
		title,
		description,
		company,
		url,
		iconUrl,
		startDate,
		endDate
	}));
};

export default createExperience;
