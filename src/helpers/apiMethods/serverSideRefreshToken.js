import axios from "axios";
import wrapAxiosRequest from "./wrapAxiosRequest";

const serverSideRefreshToken = async (token, refreshToken) => {
	return wrapAxiosRequest(axios.post(process.env.BASE_API_URL + "/api/auth/refresh", {}, {
		headers: {
			Cookie: `token=${token}; refreshToken=${refreshToken};`
		}
	}));
};

export default serverSideRefreshToken;
