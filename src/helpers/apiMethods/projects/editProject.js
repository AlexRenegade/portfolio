import axios from "../axiosWithRefreshToken";
import wrapAxiosRequest from "../wrapAxiosRequest";

const editProject = async (id, name, shortDescription, longDescription,
	openSource, repository, imageUrls, contributors, skillIds) => {
	return wrapAxiosRequest(axios.put(`/api/project/${id}`, {
		name,
		shortDescription,
		longDescription,
		openSource,
		repository,
		imageUrls,
		contributors,
		skillIds
	}));
};

export default editProject;
