import axios from "../axiosWithRefreshToken";
import wrapAxiosRequest from "../wrapAxiosRequest";

const createProject = async (name, shortDescription, longDescription,
	openSource, repository, imageUrls, contributors, skillIds) => {
	return wrapAxiosRequest(axios.post("/api/project", {
		name,
		shortDescription,
		longDescription,
		openSource,
		repository,
		imageUrls,
		contributors,
		skillIds
	}));
};

export default createProject;
