import axios from "axios";
import wrapAxiosRequest from "./wrapAxiosRequest";

const login = async (username, password) => {
	return wrapAxiosRequest(axios.post("/api/auth/login", {
		username,
		password
	}));
};

export default login;
