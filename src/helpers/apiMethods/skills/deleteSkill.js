import axios from "../axiosWithRefreshToken";
import wrapAxiosRequest from "../wrapAxiosRequest";

const deleteSkill = async (skillId) => {
	return wrapAxiosRequest(axios.delete(`/api/skill/${skillId}`));
};

export default deleteSkill;
