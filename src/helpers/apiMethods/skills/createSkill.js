import axios from "../axiosWithRefreshToken";
import wrapAxiosRequest from "../wrapAxiosRequest";

const createSkill = async (name, type, link, imageUrl, colour) => {
	return wrapAxiosRequest(axios.post("/api/skill", {
		name,
		type,
		link,
		imageUrl,
		colour
	}));
};

export default createSkill;

