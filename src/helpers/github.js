import axios from "axios";
import {getJson, setexJson} from "./redis";
import {weeklyContributionsQuery} from "./graphql/github";
import {DateTime} from "luxon";

export const getContributionStatistics = async () => {
	let cachedStats = await getJson("githubContributionStatistics");

	// Don't use cached stats for dev
	if (cachedStats && process.env.ENVIRONMENT !== "dev")
		return cachedStats;

	let response = (await axios.post("https://api.github.com/graphql", weeklyContributionsQuery, {
		headers: {
			Authorization: `Bearer ${process.env.GITHUB_AUTH_TOKEN}`
		}
	})).data;

	let {
		data: {
			viewer: {
				contributionsCollection: {
					contributionCalendar: {
						totalContributions,
						weeks
					}
				}
			}
		}
	} = response;

	let days = 0;
	let contributionStreak = 0;
	let highestDay = {
		date: "",
		count: -1
	};
	let streakBroken = false;

	for (let i = weeks.length - 1; i > 0; i--) {
		let week = weeks[i];
		let {contributionDays} = week;

		for (let j = contributionDays.length - 1; j > 0; j--) {
			let day = contributionDays[j];
			let {contributionCount, date} = day;

			if (contributionCount > 0) {
				if (!streakBroken) {
					days += 1;
					contributionStreak += contributionCount;
				}

				if (contributionCount > highestDay.count) {
					highestDay = {
						date,
						count: contributionCount
					};
				}
			} else
				streakBroken = true;
		}
	}

	let statsObject = {
		contributionStreak: {
			days,
			contributions: contributionStreak
		},
		totalContributions: totalContributions,
		highestDay
	};

	await setexJson("githubContributionStatistics", statsObject, 3600);

	return statsObject;
};

export const getLastContributionTimestamp = async () => {
	// eslint-disable-next-line max-len
	let url = `https://api.github.com/users/${process.env.GITHUB_USERNAME}/events?per_page=1`;

	let response = (await axios.get(url, {
		headers: {
			Authorization: `Bearer ${process.env.GITHUB_AUTH_TOKEN}`
		}
	})).data;

	let [latestEvent] = response;

	if (!latestEvent)
		return null;

	return DateTime.fromISO(latestEvent.created_at)
		.toMillis();
};
