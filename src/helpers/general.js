import {randomBytes} from "crypto";

const englishOrdinalRules = new Intl.PluralRules("en", {type: "ordinal"});
const ordinalSuffixes = {
	one: "st",
	two: "nd",
	few: "rd",
	other: "th"
};

export const toTitleCase = (value) => {
	return value
		.split(" ")
		.map(word => {
			// If the word is in full caps, leave it as is
			if (word === word.toUpperCase())
				return word;

			return word.toLowerCase()
				.charAt(0)
				.toUpperCase() + word.slice(1);
		})
		.join(" ");
};

export const generateId = (prefix) => {
	let id = randomBytes(8)
		.toString("hex");

	return `${prefix}_${id}`;
};

export const parseMilliseconds = (ms) => {
	let minutes = Math.floor(ms / 60000);
	let seconds = Math.floor((ms % 60000) / 1000).toString().padStart(2, "0");

	return `${minutes}:${seconds}`;
};

export const calculateTextColorFromHex = (input) => {
	let hex = input;

	if (input.startsWith("#"))
		hex = input.substr(1);

	const r = parseInt(hex.substr(0, 2), 16);
	const g = parseInt(hex.substr(2, 2), 16);
	const b = parseInt(hex.substr(4, 2), 16);

	let intensity = r * 0.299 + g * 0.587 + b * 0.114;
	if (intensity > 155)
		return "202020";

	return "ffffff";
};

export const pluralise = (value, string) => {
	if (value > 1)
		return string + "s";

	return string;
};

export const ordinal = (number) => {
	let suffix = ordinalSuffixes[englishOrdinalRules.select(number)];
	return number + suffix;
};
