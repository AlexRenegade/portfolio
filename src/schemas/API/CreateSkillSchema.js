import Joi from "joi";

export default Joi.object({
	name: Joi.string()
		.max(128)
		.required(),
	type: Joi.string()
		.max(128)
		.required(),
	link: Joi.string()
		.uri({
			scheme: ["http", "https"]
		})
		.required(),
	imageUrl: Joi.string()
		.uri({
			scheme: ["http", "https"]
		})
		.required(),
	colour: Joi.string()
		.length(6)
		.hex()
		.required()
});
