import Joi from "joi";

export default Joi.object({
	token: Joi.string()
		.regex(/^ey/)
		.required(),
	refreshToken: Joi.string()
		.regex(/^ey/)
		.required()
});
