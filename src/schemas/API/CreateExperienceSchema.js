import Joi from "joi";

export default Joi.object({
	title: Joi.string()
		.max(128)
		.required(),
	description: Joi.string()
		.max(65535)
		.required(),
	company: Joi.string()
		.max(128)
		.required(),
	url: Joi.string()
		.uri({
			scheme: ["http", "https"]
		})
		.max(128),
	iconUrl: Joi.string()
		.uri({
			scheme: ["http", "https"]
		})
		.max(256),
	startDate: Joi.number()
		.positive()
		.integer()
		.required(),
	endDate: Joi.number()
		.positive()
		.integer()
		.allow(null)
		.min(Joi.ref("startDate"))
		.message({
			// Use a custom message because by default it ends with "ref:startDate" instead of "startDate"
			"number.min": "\"endDate\" must be greater than or equal to \"startDate\""
		})
		.required()
});
