import Joi from "joi";

export default Joi.object({
	title: Joi.string()
		.max(128)
		.required()
		.label("Title"),
	description: Joi.string()
		.max(65535)
		.required()
		.label("Description"),
	company: Joi.string()
		.max(128)
		.required()
		.label("Company"),
	url: Joi.string()
		.uri({
			scheme: ["http", "https"]
		})
		.max(128)
		.label("URL"),
	iconUrl: Joi.string()
		.uri({
			scheme: ["http", "https"]
		})
		.max(256)
		.label("Icon URL"),
	startDate: Joi.number()
		.positive()
		.integer()
		.required()
		.label("Start Date"),
	endDate: Joi.number()
		.positive()
		.integer()
		.allow(null)
		.min(Joi.ref("startDate"))
		.message({
			// Use a custom message because by default it ends with "ref:startDate" instead of "startDate"
			"number.min": "\"End Date\" must be greater than or equal to \"Start Date\""
		})
		.required()
		.label("End Date")
});
