import Joi from "joi";

export default Joi.object({
	location: Joi.string()
		.max(128)
		.required()
		.label("Location"),
	level: Joi.string()
		.max(64)
		.required()
		.label("Level"),
	subject: Joi.string()
		.max(64)
		.required()
		.label("Subject"),
	startDate: Joi.number()
		.min(1900)
		.max(2100)
		.required()
		.label("Start Date"),
	endDate: Joi.number()
		.min(1900)
		.max(2100)
		.greater(Joi.ref("startDate", {
			render: true
		}))
		.label("End Date"),
	grade: Joi.string()
		.max(32)
		.required()
		.label("Grade")
});
