/* eslint-disable camelcase,no-plusplus */
const chalk = require("chalk");
const http = require("http");
const querystring = require("querystring");
const open = require("open");
const axios = require("axios");
const knex = require("knex");
const {randomBytes} = require("crypto");
const port = 8080;
const host = "localhost";
const requiredScopes = ["user-read-playback-state"];
const queryBuilder = knex({
	client: "mysql"
});

let consoleLoadingInterval;

console.log("Starting...");
const {
	SPOTIFY_CLIENT_ID: spotifyClientId,
	SPOTIFY_CLIENT_SECRET: spotifyClientSecret
} = process.env;

const generateId = (prefix) => {
	let id = randomBytes(8)
		.toString("hex");

	return `${prefix}_${id}`;
};

const consoleLoadingAnimation = (text = "") => {
	const chars = ["⠙", "⠘", "⠰", "⠴", "⠤", "⠦", "⠆", "⠃", "⠋", "⠉"];
	let x = 0;

	consoleLoadingInterval = setInterval(() => {
		process.stdout.write("\r" + chars[x++] + " " + text);
		x %= chars.length;
	}, 100);
};

const clearConsoleLoadingAnimation = () => {
	clearInterval(consoleLoadingInterval);
	process.stdout.clearLine();
	process.stdout.write("\r");
};

const returnHttpErrorResponse = (res) => {
	res.end("Error occured authenticating with spotify. Check the console for details.");
};

const exitWithErrorMessage = (message) => {
	console.log(chalk.red(message));
	process.exit();
};

const requestListener = async (req, res) => {
	let urlQuery = req.url.split("?")[1];
	let query = querystring.parse(urlQuery);

	if (!req.url.startsWith("/?")) {
		res.end("");
		return;
	}

	let {code, error} = query;

	clearConsoleLoadingAnimation();

	if (error) {
		returnHttpErrorResponse(res);
		exitWithErrorMessage(`Could not authenticate with Spotify (Error: ${error})`);
		return;
	}

	if (!code) {
		returnHttpErrorResponse(res);
		exitWithErrorMessage("Could not authenticate with Spotify (Code was missing)");
		return;
	}

	let authHeader = "Basic " + Buffer.from(`${spotifyClientId}:${spotifyClientSecret}`)
		.toString("base64");

	try {
		consoleLoadingAnimation("Requesting access token");

		let data = querystring.stringify({
			grant_type: "authorization_code",
			code,
			redirect_uri: "http://localhost:8080"
		});
		let config = {
			headers: {
				Authorization: authHeader
			}
		};

		let spotifyResponse = (await axios.post("https://accounts.spotify.com/api/token",
			data, config)).data;

		clearConsoleLoadingAnimation();

		res.end(`<div>Successfully authenticated with Spotify.
			You can now close this window.</div>
			<script>window.close()</script>`);

		let expiresAt = new Date().getTime() + (spotifyResponse.expires_in * 1000);

		let insertQuery = queryBuilder.table("credentials")
			.insert({
				id: generateId("cred"),
				type: "spotify",
				data: JSON.stringify({
					access_token: spotifyResponse.access_token,
					refresh_token: spotifyResponse.refresh_token,
					scope: spotifyResponse.scope
				}),
				expiresAt
			})
			.toQuery();

		console.log(chalk.greenBright("\nSuccessfully authenticated with Spotify"));
		console.log(chalk.cyan("Run the following query to insert your " +
			"credentials into the database.\nYou " + chalk.underline("should") +
			" only have to do this once, the token will be automatically refreshed when required.\n"));

		console.log(insertQuery);

		process.exit(0);
	} catch (err) {
		let statusCode = err.response.status;

		clearConsoleLoadingAnimation();
		returnHttpErrorResponse(res);
		exitWithErrorMessage(`Could not authenticate with Spotify (Status code ${statusCode})`);
	}
};

const server = http.createServer(requestListener);

server.listen(port, host, async () => {
	if (!spotifyClientId || !spotifyClientSecret) {
		exitWithErrorMessage("[!] SPOTIFY_CLIENT_ID and SPOTIFY_CLIENT_SECRET " +
			"are required env variables");
	}

	let spotifyAuthorizeQueryString = querystring.stringify({
		response_type: "code",
		client_id: spotifyClientId,
		scope: requiredScopes.join(" "),
		redirect_uri: "http://localhost:8080"
	});
	let url = `https://accounts.spotify.com/authorize?${spotifyAuthorizeQueryString}`;

	await open(url);

	console.log("Check your browser to authenticate with Spotify");
	consoleLoadingAnimation("Waiting for Spotify authorization");
});
