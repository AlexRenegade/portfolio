import useSWR from "swr";
import {useRouter} from "next/router";
import qs from "querystring";

const useEndpointWithQueryParams =
	(endpoint, property, params = {}, errorAsLoading = true, revalidateOnFocus = true) => {
		let url = endpoint;

		for (let key of Object.keys(params)) {
			if (params[key] === null)
				delete params[key];
		}

		let queryString = qs.stringify(params);
		url += "?" + queryString;

		const {data, error} = useSWR(url, {revalidateOnFocus});
		const router = useRouter();

		if (error) {
			if ((error.statusCode === 401 || error.statusCode === 403)
				&& error.data.code === "INVALID_AUTHENTICATION_STATUS") {
				router.push("/");
				return {loading: true};
			}
		}

		if (Array.isArray(property)) {
			let returnObject = {
				loading: errorAsLoading ? !(data && !error) : !(data || error),
				error
			};

			property.forEach(prop => {
				returnObject[prop] = data ? data[prop] : null;
			});

			return returnObject;
		}

		return {
			[property]: data ? data[property] : null,
			loading: errorAsLoading ? !(data && !error) : !(data || error),
			error
		};
	};

export default useEndpointWithQueryParams;
