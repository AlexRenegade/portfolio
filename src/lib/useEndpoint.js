import useSWR from "swr";
import { useRouter } from "next/router";

const useEndpoint = (endpoint, property, errorAsLoading = true, revalidateOnFocus = true) => {
	const { data, error } = useSWR(endpoint, { revalidateOnFocus });
	const router = useRouter();

	if (error) {
		if ((error.statusCode === 401 || error.statusCode === 403)
			&& error.data.code === "INVALID_AUTHENTICATION_STATUS") {
			router.push("/");
			return { loading: true };
		}
	}

	if (Array.isArray(property)) {
		let returnObject = {
			loading: errorAsLoading ? !(data && !error) : !(data || error),
			error
		};

		property.forEach(prop => {
			returnObject[prop] = data ? data[prop] : null;
		});

		return returnObject;
	}

	return {
		[property]: data ? data[property] : null,
		loading: errorAsLoading ? !(data && !error) : !(data || error),
		error
	};
};

export default useEndpoint;
