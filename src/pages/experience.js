import React from "react";
import SmallHeader from "../components/Header/SmallHeader";
import ExperienceList from "../components/Experience/ExperienceList";

const Experience = () => {
	return (
		<>
			<SmallHeader
				title="Experience"
			/>

			<ExperienceList/>
		</>
	);
};

export default Experience;
