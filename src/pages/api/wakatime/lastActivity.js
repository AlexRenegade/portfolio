import {internalServerError, methodNotAllowed} from "../../../lib/responses";
import {getLastActivity} from "../../../helpers/wakatime";

const handler = async (req, res) => {
	try {
		if (req.method === "GET") {
			let activityObject = await getLastActivity();

			return res.status(200).send({
				error: false,
				code: "SUCCESS",
				data: activityObject
			});
		}

		return res.status(405).send(methodNotAllowed(req.method, ["GET"]));
	} catch (err) {
		console.log(err);
		return res.status(500).send(internalServerError);
	}
};

export default handler;
