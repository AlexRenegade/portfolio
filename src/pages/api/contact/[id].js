import {
	query,
	queryBuilder
} from "../../../lib/database";
import {
	internalServerError,
	notFound,
	methodNotAllowed
} from "../../../lib/responses";
import authMiddleware from "../../../middleware/authMiddleware";

const handler = async (req, res) => {
	try {
		if (req.method === "DELETE") {
			let {id} = req.query;

			let contactQuery = queryBuilder.table("contacts")
				.select("*")
				.where({
					id
				})
				.toSQL();

			let [contact] = await query(contactQuery.sql, contactQuery.bindings);

			if (!contact)
				return res.status(404).send(notFound("Contact"));

			let deleteQuery = queryBuilder.table("contacts")
				.del()
				.where({
					id
				})
				.toSQL();

			await query(deleteQuery.sql, deleteQuery.bindings);

			return res.status(204).send();
		}

		return res.status(405).send(methodNotAllowed(req.method, ["DELETE"]));
	} catch (err) {
		console.log(err);
		return res.status(500).send(internalServerError);
	}
};

export default authMiddleware(handler);
