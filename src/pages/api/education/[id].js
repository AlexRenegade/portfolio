import {
	query,
	queryBuilder
} from "../../../lib/database";
import {
	internalServerError,
	notFound,
	methodNotAllowed
} from "../../../lib/responses";
import authMiddleware from "../../../middleware/authMiddleware";

const handler = async (req, res) => {
	try {
		if (req.method === "DELETE") {
			let {id} = req.query;

			let educationQuery = queryBuilder.table("education")
				.select("*")
				.where({
					id
				})
				.toSQL();

			let [education] = await query(educationQuery.sql, educationQuery.bindings);

			if (!education)
				return res.status(404).send(notFound("Education"));

			let deleteQuery = queryBuilder.table("education")
				.del()
				.where({
					id
				})
				.toSQL();

			await query(deleteQuery.sql, deleteQuery.bindings);

			return res.status(204).send();
		}

		return res.status(405).send(methodNotAllowed(req.method, ["DELETE"]));
	} catch (err) {
		console.log(err);
		return res.status(500).send(internalServerError);
	}
};

export default authMiddleware(handler);
