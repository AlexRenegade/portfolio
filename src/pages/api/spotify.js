/* eslint-disable camelcase */
import SpotifyWebApi from "spotify-web-api-node";
import {query, queryBuilder} from "../../lib/database";
import {internalServerError, methodNotAllowed} from "../../lib/responses";

const spotifyApi = new SpotifyWebApi({
	clientId: process.env.SPOTIFY_CLIENT_ID,
	clientSecret: process.env.SPOTIFY_CLIENT_SECRET,
	redirectUri: process.env.SPOTIFY_REDIRECT_URI
});

const handler = async (req, res) => {
	try {
		if (req.method === "GET") {
			let credQuery = queryBuilder.table("credentials")
				.select("*")
				.where({
					type: "spotify"
				})
				.toSQL();

			let [creds] = await query(credQuery.sql, credQuery.bindings);

			if (!creds) {
				return res.status(401).send({
					error: true,
					displayMessage: "Spotify credentials could not be found to fetch data",
					code: "CREDS_NOT_FOUND"
				});
			}

			let credData = JSON.parse(creds.data);
			spotifyApi.setAccessToken(credData.access_token);
			spotifyApi.setRefreshToken(credData.refresh_token);

			if (creds.expiresAt < new Date().getTime()) {
				let refreshResponse = (await spotifyApi.refreshAccessToken()).body;

				credData.access_token = refreshResponse.access_token;
				spotifyApi.setAccessToken(credData.access_token);

				if (refreshResponse.refresh_token) {
					credData.refresh_token = refreshResponse.refresh_token;
					spotifyApi.setRefreshToken(credData.refresh_token);
				}

				let updateObject = {
					data: JSON.stringify(credData),
					expiresAt: new Date().getTime() + (refreshResponse.expires_in * 1000)
				};

				let updateQuery = queryBuilder.table("credentials")
					.update(updateObject)
					.where({
						id: creds.id
					})
					.toSQL();

				await query(updateQuery.sql, updateQuery.bindings);
			}

			let currentPlaybackResponse = (await spotifyApi.getMyCurrentPlaybackState()).body;

			if (!currentPlaybackResponse.is_playing) {
				return res.status(200).send({
					error: false,
					code: "SUCCESS",
					data: {
						playing: false
					}
				});
			}

			let albumCover = null;

			if (currentPlaybackResponse.item.album) {
				if (currentPlaybackResponse.item.album.images[0])
					albumCover = currentPlaybackResponse.item.album.images[0].url;
			}

			return res.status(200).send({
				error: false,
				code: "SUCCESS",
				data: {
					playing: currentPlaybackResponse.is_playing,
					track: {
						name: currentPlaybackResponse.item.name,
						url: currentPlaybackResponse.item.external_urls.spotify
					},
					artists: currentPlaybackResponse.item.artists.map(artist => {
						return {
							name: artist.name,
							url: artist.external_urls.spotify
						};
					}),
					albumCover,
					progress: {
						current: currentPlaybackResponse.progress_ms,
						duration: currentPlaybackResponse.item.duration_ms
					}
				}
			});
		}

		return res.status(405).send(methodNotAllowed(req.method, ["GET"]));
	} catch (err) {
		console.log(err);
		return res.status(500).send(internalServerError);
	}
};

export default handler;
