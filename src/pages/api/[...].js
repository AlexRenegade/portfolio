const handler = async (req, res) => {
	return res.status(404).send({
		error: true,
		displayMessage: "Route not found",
		code: "NOT_FOUND"
	});
};

export default handler;
