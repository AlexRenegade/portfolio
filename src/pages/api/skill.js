import {
	internalServerError,
	methodNotAllowed
} from "../../lib/responses";
import {
	query,
	queryBuilder
} from "../../lib/database";
import {generateId, toTitleCase} from "../../helpers/general";
import authMiddleware from "../../middleware/authMiddleware";
import CreateSkillSchema from "../../schemas/API/CreateSkillSchema";

const handler = async (req, res) => {
	try {
		if (req.method === "GET") {
			let resultsQuery = queryBuilder.table("skills")
				.select("*")
				.toSQL();

			let skillCountQuery = queryBuilder.table("projectSkills")
				.select("skillId")
				.count("skillId as count")
				.groupBy("skillId")
				.toSQL();

			let [skills, skillCounts] = await Promise.all([
				query(resultsQuery.sql, resultsQuery.bindings),
				query(skillCountQuery.sql, skillCountQuery.bindings)
			]);

			skills.forEach(skill => {
				let skillCount = skillCounts.find(x => x.skillId === skill.id);
				skill.projectCount = skillCount ? skillCount.count : 0;
			});

			return res.status(200).send({
				error: false,
				code: "SUCCESS",
				skills
			});
		}

		if (req.method === "POST") {
			let {body} = req;

			if (!body) {
				return res.status(400).send({
					error: true,
					displayMessage: "Missing body",
					code: "MISSING_BODY"
				});
			}

			if (Object.keys(body).length === 0) {
				return res.status(400).send({
					error: true,
					displayMessage: "Request body is empty",
					code: "EMPTY_BODY"
				});
			}

			let validationResult = CreateSkillSchema.validate(body, {
				abortEarly: false,
				stripUnknown: true
			});

			if (validationResult.error) {
				return res.status(400).send({
					error: true,
					displayMessage: "There are validation errors within the body",
					errors: validationResult.error.details,
					code: "BODY_VALIDATION_ERROR"
				});
			}

			let {
				name,
				type,
				link,
				imageUrl,
				colour
			} = validationResult.value;

			let existingSkillQuery = queryBuilder.table("skills")
				.select("id")
				.where({
					name
				})
				.toSQL();

			let [existingSkill] = await query(existingSkillQuery.sql, existingSkillQuery.bindings);

			if (existingSkill) {
				return res.status(409).send({
					error: true,
					displayMessage: "A skill with that name already exists",
					code: "SKILL_EXISTS"
				});
			}

			let skill = {
				id: generateId("skill"),
				name,
				type: toTitleCase(type),
				link,
				imageUrl,
				colour
			};

			let insertQuery = queryBuilder.table("skills")
				.insert(skill)
				.toSQL();

			await query(insertQuery.sql, insertQuery.bindings);

			return res.status(200).send({
				error: false,
				code: "SUCCESS",
				skill
			});
		}

		return res.status(405).send(methodNotAllowed(req.method, ["POST"]));
	} catch (err) {
		console.log(err);
		return res.status(500).send(internalServerError);
	}
};

export default authMiddleware(handler, ["GET"]);
