import {internalServerError, methodNotAllowed} from "../../lib/responses";
import {getContributionStatistics, getLastContributionTimestamp} from "../../helpers/github";

const handler = async (req, res) => {
	try {
		if (req.method === "GET") {
			let contributionsStats = await getContributionStatistics();
			let lastContributionTimestamp = await getLastContributionTimestamp();

			return res.status(200).send({
				error: false,
				code: "SUCCESS",
				data: {
					...contributionsStats,
					lastContributionTimestamp
				}
			});
		}

		return res.status(405).send(methodNotAllowed(req.method, ["GET"]));
	} catch (err) {
		console.log(err);
		return res.status(500).send(internalServerError);
	}
};

export default handler;
