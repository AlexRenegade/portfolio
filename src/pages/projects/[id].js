import React from "react";
import {useRouter} from "next/router";
import useEndpoint from "../../lib/useEndpoint";
import ProjectPage from "../../components/Projects/ProjectPage";
import ErrorPage from "../../components/Errors/ErrorPage";

const ViewProjectPage = () => {
	const router = useRouter();
	const {id} = router.query;

	if (!id)
		return null;

	const {project, loading: projectLoading, error} = useEndpoint(`/api/project/${id}`, "project");

	if (error)
		return <ErrorPage statusCode={error.statusCode}/>;

	return <ProjectPage
		loading={projectLoading}
		project={{...project}}
	/>;
};

export default ViewProjectPage;
