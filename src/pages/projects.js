import React from "react";
import SmallHeader from "../components/Header/SmallHeader";
import ProjectList from "../components/Projects/ProjectList";

const Projects = () => {
	return (
		<>
			<SmallHeader
				title="Projects"
				subtitle="A list of projects which I have worked on"
			/>

			<ProjectList/>
		</>
	);
};

export default Projects;
