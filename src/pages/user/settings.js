import React from "react";
import SmallHeader from "../../components/Header/SmallHeader";
import UserSettings from "../../components/User/UserSettings";

const Settings = () => {
	return (
		<>
			<SmallHeader
				title="User Settings"
			/>

			<UserSettings/>
		</>
	);
};

export default Settings;
