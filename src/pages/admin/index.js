import React from "react";
import SmallHeader from "../../components/Header/SmallHeader";
import AdminPageList from "../../components/Admin/AdminPage/AdminPageList";
import {useAuth} from "../../lib/useAuth";
import ErrorPage from "../../components/Errors/ErrorPage";

const Index = ({error}) => {
	if (error)
		return <ErrorPage statusCode={error}/>;

	return (
		<>
			<SmallHeader
				title="Admin Dashboard"
				subtitle="View and manage resources"
			/>

			<AdminPageList/>
		</>
	);
};

export const getServerSideProps = async (ctx) => {
	const data = await useAuth(ctx);

	if (!data)
		return {props: {error: 404}};

	return {props: {}};
};

export default Index;
