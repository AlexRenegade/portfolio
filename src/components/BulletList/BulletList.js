import React from "react";

const BulletList = ({children, className}) => {
	return (
		<ul className={"list-disc space-y-2 " + className}>
			{children}
		</ul>
	);
};

export default BulletList;
