import React from "react";

const Link = ({children, href, text, target = "_blank", textClass, title}) => {
	return (
		<a href={href} target={target} className={textClass} title={title}>
			{text}
			{children}
		</a>
	);
};

export default Link;
