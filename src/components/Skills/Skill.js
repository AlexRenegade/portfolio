import React from "react";
import SkeletonImage from "../Image/SkeletonImage";
import SkillTag from "./SkillTag";
import LinkButton from "../Button/LinkButton";

const Skill = ({id, imageUrl, name, colour, projectCount}) => {
	let skillObject = {
		id,
		name,
		imageUrl,
		colour
	};
	let base64Object = Buffer.from(JSON.stringify(skillObject)).toString("base64");

	return (
		<div className="p-1">
			<div className="bg-dark-650 p-6 rounded-lg text-white">
				<SkeletonImage
					className="lg:h-50 xl:h-32 md:h-42 sm:h-72 xs:h-64 h-64
					rounded w-auto object-cover object-center mb-6 mx-auto"
					src={imageUrl}
					alt={`${name} image`}
				/>

				<div className="flex flex-wrap justify-center mb-6">
					<SkillTag
						text={name}
						colour={colour}
						clickable={false}
					/>
				</div>

				<LinkButton href={`/projects?skill=${base64Object}`}>
					View usage in {projectCount} project{projectCount === 1 ? "" : "s"}
				</LinkButton>
			</div>
		</div>
	);
};

export default Skill;
