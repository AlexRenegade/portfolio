import React from "react";
import Skeleton from "react-loading-skeleton";
import useEndpoint from "../../lib/useEndpoint";
import SkillGroup from "./SkillGroup";

const SkillsList = () => {
	const {skills, loading: skillsLoading} = useEndpoint("/api/skill", "skills", true, false);

	if (skillsLoading) {
		return (
			<div className="grid grid-cols-1 text-center px-8 gap-4
				lg:grid-cols-3 2xl:grid-cols-4 lg:px-32">
				{Array(6).fill(null).map((d, i) => {
					return <Skeleton
						key={i}
						height="12rem"
					/>;
				})}
			</div>
		);
	}

	let skillGroups = {};

	skills.forEach(skill => {
		if (!skillGroups[skill.type])
			skillGroups[skill.type] = [];

		skillGroups[skill.type].push(skill);
	});

	return (
		Object.keys(skillGroups).sort().map(groupName => {
			return <SkillGroup
				key={groupName}
				title={groupName}
				skills={skillGroups[groupName]}
			/>;
		})
	);
};

export default SkillsList;
