import React from "react";
import SubHeader from "../Header/SubHeader";
import Skill from "./Skill";

const SkillGroup = ({title, skills}) => {
	return (
		<>
			<SubHeader
				title={title}
			/>

			<div className="grid grid-cols-1 text-center px-8 gap-4
				lg:grid-cols-3 2xl:grid-cols-4 lg:px-32 mb-4">
				{skills.map(skill => {
					return <Skill
						key={skill.id}
						{...skill}
					/>;
				})}
			</div>
		</>
	);
};

export default SkillGroup;
