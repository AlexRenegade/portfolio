import React, {useState} from "react";
import TextInput from "../FormElements/TextInput";
import {
	LinkIcon,
	PencilAltIcon,
	PhotographIcon,
	ViewListIcon
} from "@heroicons/react/solid";
import ErrorList from "../Error/ErrorList";
import SubmitButton from "../FormElements/SubmitButton";
import ColourPickerButton from "../ColourPicker/ColourPickerButton";
import {useRouter} from "next/router";
import createContact from "../../helpers/apiMethods/contacts/createContact";
import CreateContactSchema from "../../schemas/Frontend/CreateContactSchema";

const NewContactForm = ({onChange}) => {
	const router = useRouter();
	const [loading, setLoading] = useState(false);
	const [errors, setErrors] = useState([]);
	const [colour, setColour] = useState("ffffff");

	const handleOnInput = (event) => {
		let {name, value} = event.target;

		onChange({name, value});
	};

	const handleColourChange = (newColour) => {
		setColour(newColour);

		onChange({name: "iconColour", value: newColour});
	};

	const handleSubmit = async (event) => {
		event.preventDefault();

		let {
			title: titleElement,
			text: textElement,
			link: linkElement,
			baseSiteLink: baseSiteLinkElement,
			icon: iconElement
		} = event.target;

		let title = titleElement.value;
		let text = textElement.value;
		let link = linkElement.value || null;
		let baseSiteLink = baseSiteLinkElement.value;
		let icon = iconElement.value;

		let validationResult = CreateContactSchema.validate({
			title,
			text,
			link,
			baseSiteLink,
			icon,
			iconColour: colour
		}, {
			abortEarly: false,
			stripUnknown: true
		});

		if (validationResult.error) {
			setErrors(validationResult.error.details);
			return;
		}

		({
			title,
			text,
			link,
			baseSiteLink,
			icon
		} = validationResult.value);

		setErrors([]);
		setLoading(true);

		let {error} = await createContact(title, text, link, baseSiteLink, icon, colour);

		setLoading(false);

		if (error) {
			setErrors([{message: error.displayMessage}]);
			return;
		}

		await router.push("/admin/contacts?addedContact=1");
	};

	return (
		<div className="flex justify-center py-12 px-0 lg:px-8">
			<div className="max-w-md w-full space-y-8">
				<form className="mt-8 space-y-4" onSubmit={handleSubmit}>
					<TextInput
						name="title"
						placeholder="Title"
						position="alone"
						autoComplete="off"
						icon={PencilAltIcon}
						onInput={handleOnInput}
					/>

					<TextInput
						name="text"
						placeholder="Text"
						position="alone"
						autoComplete="off"
						icon={ViewListIcon}
						onInput={handleOnInput}
					/>

					<TextInput
						name="link"
						placeholder="Link"
						position="alone"
						autoComplete="off"
						icon={LinkIcon}
						onInput={handleOnInput}
					/>

					<TextInput
						name="baseSiteLink"
						placeholder="Base Site Link"
						position="alone"
						autoComplete="off"
						icon={LinkIcon}
						onInput={handleOnInput}
					/>

					<TextInput
						name="icon"
						placeholder="Icon"
						position="alone"
						autoComplete="off"
						icon={PhotographIcon}
						onInput={handleOnInput}
					/>

					<ColourPickerButton
						name="colour"
						colour={colour}
						setColour={handleColourChange}
					/>

					<ErrorList errors={errors}/>

					<SubmitButton
						text={loading ? "Creating..." : "Create"}
						loading={loading}
					/>
				</form>
			</div>
		</div>
	);
};

export default NewContactForm;
