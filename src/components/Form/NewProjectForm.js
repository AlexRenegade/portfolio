import React, {useRef, useState} from "react";
import TextInput from "../FormElements/TextInput";
import {
	ClipboardIcon,
	CodeIcon,
	PencilAltIcon,
	ViewListIcon
} from "@heroicons/react/solid";
import ErrorList from "../Error/ErrorList";
import SubmitButton from "../FormElements/SubmitButton";
import {useRouter} from "next/router";
import TextAreaInput from "../FormElements/TextAreaInput";
import CreateProjectSchema from "../../schemas/Frontend/CreateProjectSchema";
import createProject from "../../helpers/apiMethods/projects/createProject";
import Checkbox from "../FormElements/Checkbox";
import EditableSkillsList from "../Admin/Projects/EditableSkillsList";
import EditableContributorsList from "../Admin/Projects/EditableContributorsList";
import EditableImagesList from "../Admin/Projects/EditableImagesList";

const NewProjectForm = ({onChange}) => {
	const router = useRouter();
	const [loading, setLoading] = useState(false);
	const [errors, setErrors] = useState([]);
	const selectedSkillIds = useRef([]);
	const addedContributors = useRef([]);
	const addedImages = useRef([]);

	const handleOnInput = (event) => {
		let {name, value} = event.target;

		onChange({name, value});
	};

	const handleOnChecked = (event) => {
		let {name, checked} = event.target;

		onChange({name, value: checked});
	};

	const handleSkillsListChanged = (newSkills) => {
		selectedSkillIds.current = newSkills;
	};

	const handleContributorsChanged = (newContributors) => {
		addedContributors.current = newContributors;
	};

	const handleImagesListChanged = (newImages) => {
		addedImages.current = newImages;
	};

	const handleSubmit = async (event) => {
		event.preventDefault();

		let {
			name: nameElement,
			shortDescription: shortDescriptionElement,
			longDescription: longDescriptionElement,
			repository: repositoryElement,
			openSource: openSourceElement
		} = event.target;

		let name = nameElement.value;
		let shortDescription = shortDescriptionElement.value;
		let longDescription = longDescriptionElement.value;
		let repository = repositoryElement.value;
		let openSource = openSourceElement.checked;
		let imageUrls = addedImages.current;
		let contributors = addedContributors.current;
		let skillIds = selectedSkillIds.current;

		let validationResult = CreateProjectSchema.validate({
			name,
			shortDescription,
			longDescription,
			repository,
			openSource,
			imageUrls,
			contributors,
			skillIds
		}, {
			abortEarly: false,
			stripUnknown: true
		});

		if (validationResult.error) {
			setErrors(validationResult.error.details);
			return;
		}

		({
			name,
			shortDescription,
			longDescription,
			repository,
			openSource,
			imageUrls,
			contributors,
			skillIds
		} = validationResult.value);

		setErrors([]);
		setLoading(true);

		let {error} = await createProject(name, shortDescription,
			longDescription, openSource, repository, imageUrls,
			contributors, skillIds);

		setLoading(false);

		if (error) {
			if (error.code === "BODY_VALIDATION_ERROR") {
				setErrors(error.errors);
				return;
			}

			setErrors([{message: error.displayMessage}]);
			return;
		}

		await router.push("/admin/projects?addedProject=1");
	};

	return (
		<div className="flex justify-center py-12 px-0 lg:px-8">
			<div className="max-w-md w-full space-y-8">
				<form className="mt-8" onSubmit={handleSubmit}>
					<div className="space-y-4">
						<TextInput
							name="name"
							placeholder="Name"
							position="alone"
							autoComplete="off"
							icon={PencilAltIcon}
							onInput={handleOnInput}
						/>

						<TextInput
							name="shortDescription"
							placeholder="Short Description"
							position="alone"
							autoComplete="off"
							icon={ClipboardIcon}
							onInput={handleOnInput}
						/>

						<TextAreaInput
							name="longDescription"
							placeholder="Long Description"
							position="alone"
							autoComplete="off"
							icon={ViewListIcon}
							onInput={handleOnInput}
							rows={5}
						/>

						<TextInput
							name="repository"
							placeholder="Repository"
							position="alone"
							autoComplete="off"
							icon={CodeIcon}
							onInput={handleOnInput}
						/>

						<EditableImagesList
							onChange={handleImagesListChanged}
						/>

						<Checkbox
							name="openSource"
							text="Open Source"
							colour="indigo"
							onChecked={handleOnChecked}
						/>
					</div>

					<EditableSkillsList
						onChange={handleSkillsListChanged}
					/>

					<EditableContributorsList
						onChange={handleContributorsChanged}
					/>

					<ErrorList errors={errors}/>

					<div className="mt-2">
						<SubmitButton
							text={loading ? "Creating..." : "Create"}
							loading={loading}
						/>
					</div>
				</form>
			</div>
		</div>
	);
};

export default NewProjectForm;
