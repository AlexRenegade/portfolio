import React from "react";
import Label from "../FormElements/Label";

const ProjectProperty = ({name, children}) => {
	return (
		<div>
			<Label>
				{name}
			</Label>
			{children}
		</div>
	);
};

export default ProjectProperty;
