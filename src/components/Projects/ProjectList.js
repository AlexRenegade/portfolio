import React, {useEffect, useState} from "react";
import Project from "./Project";
import Skeleton from "react-loading-skeleton";
import useEndpointWithQueryParams from "../../lib/useEndpointWithQueryParams";
import SkillTag from "../Skills/SkillTag";
import PageControls from "../PageControls/PageControls";
import SortByControls from "../SortByControls/SortByControls";
import {useRouter} from "next/router";

const ProjectList = () => {
	const router = useRouter();
	const [pageInfo, setPageInfo] = useState({
		totalResults: 0,
		page: 0,
		totalPages: 0,
		perPage: 0
	});
	const [filteredSkill, setFilteredSkill] = useState(null);
	const [page, setPage] = useState(1);
	const [sortBy, setSortBy] = useState("dateAdded");
	const [sortDirection, setSortDirection] = useState("DESC");

	const {
		projects, pageInfo: currentPageInfo,
		loading: projectsLoading
	} = useEndpointWithQueryParams("/api/project",
		["projects", "pageInfo"], {
			skillId: filteredSkill ? filteredSkill.id : null,
			page,
			sort: sortBy,
			sortDirection
		});

	useEffect(async () => {
		let {skill} = router.query;

		if (skill) {
			let parsedSkill = JSON.parse(Buffer.from(skill, "base64"));
			setFilteredSkill(parsedSkill);
			window.history.replaceState(null, null, "/projects");
		}
	}, []);

	useEffect(() => {
		if (currentPageInfo)
			setPageInfo(currentPageInfo);
	}, [currentPageInfo]);

	const handleTagClicked = (skill) => {
		setFilteredSkill(skill);
	};

	const handleRemoveTagFilter = () => {
		setFilteredSkill(null);
	};

	return (
		<>
			<PageControls
				currentPage={page}
				totalPages={pageInfo.totalPages}
				onPageChange={(newPageNumber) => setPage(newPageNumber)}
				totalResults={pageInfo.totalResults}
				perPage={pageInfo.perPage}
			/>

			<SortByControls
				onSortChange={(newValue) => setSortBy(newValue.value)}
				onDirectionChange={(newValue => setSortDirection(newValue.value))}
			/>

			{filteredSkill ?
				<p className="text-xl text-gray-200 px-8 gap-4 mb-4 ml-3 lg:grid-cols-3 lg:px-32">
					<span className="mr-2">
						Showing projects with
					</span>
					<SkillTag
						skillId={filteredSkill.id}
						text={filteredSkill.name}
						colour={filteredSkill.colour}
						showDeleteIcon={true}
						onDelete={handleRemoveTagFilter}
					/>
				</p> : ""
			}

			<div className="grid grid-cols-1 text-center px-8 gap-4
				lg:grid-cols-3 2xl:grid-cols-4 lg:px-32">
				{/* 12 since it makes full rows for 3 and 4 columns depending on screen size */}
				{projectsLoading ? Array(12).fill(null).map((d, i) => {
					return <Skeleton
						key={i}
						height="12rem"
					/>;
				}) : ""}

				{!projectsLoading ?
					projects.map(project => {
						let skillIds = project.skills.map(x => x.skillId);

						return <Project
							key={project.id}
							id={project.id}
							name={project.name}
							shortDescription={project.shortDescription}
							imageUrls={project.imageUrls}
							createdAt={project.dateAdded}
							openSource={project.openSource}
							skillIds={skillIds}
							onTagClick={handleTagClicked}
						/>;
					})
					: ""
				}
			</div>

			<PageControls
				currentPage={page}
				totalPages={pageInfo.totalPages}
				onPageChange={(newPageNumber) => setPage(newPageNumber)}
				totalResults={pageInfo.totalResults}
				perPage={pageInfo.perPage}
			/>
		</>
	);
};

export default ProjectList;
