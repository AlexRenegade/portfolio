import React from "react";
import Error from "./Error";

const ErrorList = ({errors}) => {
	return (
		errors.map(error => {
			return <Error key={error.message} message={error.message}/>;
		})
	);
};

export default ErrorList;
