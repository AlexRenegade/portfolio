import React from "react";
import TableHeader from "../../Table/TableHeader";
import TableRow from "../../Table/TableRow";
import TableCell from "../../Table/TableCell";
import Link from "../../Link/Link";
import Button from "../../Button/Button";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {library} from "@fortawesome/fontawesome-svg-core";
import {fab} from "@fortawesome/free-brands-svg-icons";
library.add(fab);

const AdminContactsTable = ({contacts, onContactDeleteClick}) => {
	return (
		<div className="flex flex-col mx-4 mb-6 mt-6 lg:mx-32">
			<div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
				<div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
					<div className="shadow overflow-hidden border-b border-dark-200 rounded-lg">
						<table className="min-w-full divide-y divide-dark-200">
							<thead>
								<tr>
									<TableHeader
										text="Title"
									/>
									<TableHeader
										text="Text"
									/>
									<TableHeader
										text="Link"
									/>
									<TableHeader
										text="Actions"
									/>
								</tr>
							</thead>
							<tbody className="divide-y divide-dark-200">
								{contacts.map((contact) => {
									let [iconType, iconName] = contact.icon.split(" ");

									return (
										<TableRow key={contact.id}>
											<TableCell>
												<div className="flex flex-shrink-0 h-10 w-10 justify-center items-center">
													<FontAwesomeIcon
														icon={[iconType, iconName]}
														color={"#" + contact.iconColour}
														style={{
															width: "2rem",
															height: "2rem"
														}}
													/>
												</div>
												<div className="ml-4">
													<div className="text-sm font-medium">
														{contact.title}
													</div>
												</div>
											</TableCell>

											<TableCell>
												{contact.text}
											</TableCell>

											<TableCell>
												<Link href={contact.link} textClass="text-blue-400">
													{contact.link}
												</Link>
											</TableCell>

											<TableCell>
												<Button
													colour="red"
													textColour="white"
													onClick={() => onContactDeleteClick(contact.id)}
												>
													Delete
												</Button>
											</TableCell>
										</TableRow>
									);
								})}
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	);
};

export default AdminContactsTable;
