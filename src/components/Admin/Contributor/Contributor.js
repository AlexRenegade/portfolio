import React from "react";
import Link from "next/link";
import {XIcon} from "@heroicons/react/solid";

const Contributor = ({name, link, onDelete, showDeleteIcon}) => {
	return (
		<>
			<Link href={link} passHref>
				<a className="text-blue-400" target="_blank">
					{name}
				</a>
			</Link>

			{showDeleteIcon ?
				<XIcon className="h-6 w-6 inline-block ml-2 text-red-500 cursor-pointer"
					onClick={onDelete}/> :
				""
			}
		</>
	);
};

export default Contributor;
