import React from "react";
import SubHeader from "../../Header/SubHeader";
import AdminSkillsTable from "./AdminSkillsTable";

const AdminSkillGroup = ({title, skills, onSkillDeleteClick}) => {
	return (
		<>
			<SubHeader
				title={title}
			/>

			<AdminSkillsTable
				skills={skills}
				onSkillDeleteClick={onSkillDeleteClick}
			/>
		</>
	);
};

export default AdminSkillGroup;
