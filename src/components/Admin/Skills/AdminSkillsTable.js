import React from "react";
import TableHeader from "../../Table/TableHeader";
import TableRow from "../../Table/TableRow";
import TableCell from "../../Table/TableCell";
import Link from "next/link";
import Button from "../../Button/Button";

const AdminSkillsTable = ({skills, onSkillDeleteClick}) => {
	return (
		<div className="flex flex-col mx-4 mb-6 mt-6 lg:mx-32">
			<div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
				<div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
					<div className="shadow overflow-hidden border-b border-dark-200 rounded-lg">
						<table className="min-w-full divide-y divide-dark-200">
							<thead>
								<tr>
									<TableHeader
										text="Name"
									/>
									<TableHeader
										text="Link"
									/>
									<TableHeader
										text="Project Count"
									/>
									<TableHeader
										text="Actions"
									/>
								</tr>
							</thead>
							<tbody className="divide-y divide-dark-200">
								{skills.map((skill) => (
									<TableRow key={skill.id}>
										<TableCell>
											<div className="flex-shrink-0 h-10 w-10">
												<img className="object-cover object-center" src={skill.imageUrl} alt=""/>
											</div>
											<div className="ml-4">
												<div className="text-sm font-medium">
													{skill.name}
												</div>
											</div>
										</TableCell>

										<TableCell>
											<Link href={skill.link}>
												{skill.link}
											</Link>
										</TableCell>

										<TableCell>
											{skill.projectCount}
										</TableCell>

										<TableCell>
											<Button
												colour="yellow"
												textColour="gray-800"
												href={`/admin/skills/edit/${skill.id}`}
											>
												Edit
											</Button>
											<Button
												colour="red"
												textColour="white"
												onClick={() => onSkillDeleteClick(skill.id)}
											>
												Delete
											</Button>
										</TableCell>
									</TableRow>
								))}
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	);
};

export default AdminSkillsTable;
