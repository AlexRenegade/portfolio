import React, {useEffect, useState} from "react";
import {useRouter} from "next/router";
import useEndpoint from "../../../lib/useEndpoint";
import Skill from "../../Skills/Skill";
import EditSkillForm from "../../Form/EditSkillForm";
import Skeleton from "react-loading-skeleton";

const EditSkill = () => {
	const router = useRouter();
	let {id} = router.query;

	if (!id)
		return null;

	const [skill, setSkill] = useState(null);
	const {skill: currentSkill, loading: skillsLoading} = useEndpoint(`/api/skill/${id}`, "skill",
		true, false);

	useEffect(() => {
		if (currentSkill)
			setSkill(currentSkill);
	}, [currentSkill]);

	if (skillsLoading || !skill) {
		return (
			<div className="grid grid-cols-1 text-center px-8 gap-4 mb-4
				lg:grid-cols-2 lg:px-24">

				<div className="flex justify-center py-12 px-0 lg:px-8">
					<div className="max-w-md w-full space-y-8 mt-8">
						<Skeleton height="2.875rem" className="my-2"/>
						<Skeleton height="2.875rem" className="my-2"/>
						<Skeleton height="2.875rem" className="my-2"/>
						<Skeleton height="2.875rem" className="my-2"/>
						<Skeleton height="3rem" className="my-2"/>
						<Skeleton height="2.375rem" className="my-2"/>
					</div>
				</div>

				<div>
					<h1 className="text-3xl font font-extrabold tracking-tight text-white mb-4">
						Preview
					</h1>

					<h1 className="mb-4 ml-4">
						<Skeleton width="30%"/>
					</h1>
					<Skeleton height="20rem"/>
				</div>
			</div>
		);
	}

	return (
		<div className="grid grid-cols-1 text-center px-8 gap-4 mb-4
				lg:grid-cols-2 lg:px-24">

			<EditSkillForm
				onChange={(newValue) => setSkill({
					...skill,
					[newValue.name]: newValue.value
				})}
				baseSkill={skill}
			/>

			<div>
				<h1 className="text-3xl font font-extrabold tracking-tight text-white mb-4">
					Preview
				</h1>

				<h1 className="text-2xl text-left font font-extrabold tracking-tight text-white mb-4 ml-4">
					{skill.type}
				</h1>
				<Skill {...skill}/>
			</div>
		</div>
	);
};

export default EditSkill;
