import React, {useState} from "react";
import {DateTime} from "luxon";
import {BookOpenIcon} from "@heroicons/react/outline";
import TimelineElement from "../../Timeline/TimelineElement";
import NewEducationForm from "../../Form/NewEducationForm";
import RelativeDate from "../../Timeline/Date/RelativeDate";

const NewEducation = () => {
	const [newEducation, setNewEducation] = useState({
		location: "",
		level: "",
		subject: "",
		startDate: 1900,
		endDate: 1900,
		grade: ""
	});

	let dateElement = <RelativeDate
		startDate={DateTime.fromObject({year: newEducation.startDate})}
		startDateString={newEducation.startDate}
		endDate={newEducation.endDate ?
			DateTime.fromObject({year: newEducation.endDate}) : null
		}
		endDateString={newEducation.endDate}
	/>;

	return (
		<div className="grid grid-cols-1 text-center px-8 gap-4 mb-4
				lg:grid-cols-2 lg:px-24">

			<NewEducationForm
				onChange={(newValue) => setNewEducation({
					...newEducation,
					[newValue.name]: newValue.value
				})}
			/>

			<div>
				<h1 className="text-3xl font font-extrabold tracking-tight text-white mb-4">
					Preview
				</h1>

				<TimelineElement
					key={newEducation.id}
					backgroundColour="#252525"
					iconBackgroundColour="#000000"
					iconElement={<BookOpenIcon/>}
					startDateString={newEducation.startDate}
					endDate={newEducation.endDate}
					dateElement={dateElement}
					title={newEducation.level}
					location={newEducation.location}
					body={`${newEducation.grade} in ${newEducation.subject}`}
				/>
			</div>
		</div>
	);
};

export default NewEducation;
