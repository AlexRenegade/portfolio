import React from "react";
import AdminPageCard from "./AdminPageCard";
import {adminPages} from "../../../lib/constants";

const AdminPageList = () => {
	return (
		<div className="grid grid-cols-1 text-center px-8 gap-4 mb-16 lg:grid-cols-3 lg:px-32">
			{adminPages.map(page => {
				return <AdminPageCard
					key={page.url}
					title={page.name}
					body={page.description}
					url={page.url}
					icon={page.icon}
				/>;
			})}
		</div>
	);
};

export default AdminPageList;
