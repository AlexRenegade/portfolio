import React from "react";
import ModalWithIcon from "./ModalWithIcon";
import Button from "../Button/Button";

const DeleteSkillModal = ({isOpen, onClose, onDelete, deleteLoading}) => {
	let buttons = <>
		<Button
			colour="red"
			textColour="white"
			onClick={onDelete}
			loading={deleteLoading}
		>
			Delete
		</Button>
		<Button
			colour="gray"
			textColour="white"
			onClick={onClose}
			disabled={deleteLoading}
		>
			Cancel
		</Button>
	</>;

	return (
		<ModalWithIcon
			title="Delete Skill"
			body="Are you sure you want to delete this skill? This action cannot be reversed!"
			isOpen={isOpen}
			onClose={deleteLoading ? () => {} : onClose}
			buttons={buttons}
		/>
	);
};

export default DeleteSkillModal;
