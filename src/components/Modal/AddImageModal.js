import React, {useState} from "react";
import Modal from "./Modal";
import {PhotographIcon} from "@heroicons/react/solid";
import TextInput from "../FormElements/TextInput";
import SubmitButton from "../FormElements/SubmitButton";
import ErrorList from "../Error/ErrorList";
import Joi from "joi";

const AddImageModal = ({isOpen, onClose, onAdd}) => {
	const [errors, setErrors] = useState([]);

	const handleSubmit = (event) => {
		event.preventDefault();
		event.stopPropagation();

		let {
			imageUrl: imageUrlElement
		} = event.target;

		let imageUrl = imageUrlElement.value;

		let validationResult = Joi.string()
			.uri({scheme: ["http", "https"]})
			.label("Image URL")
			.validate(imageUrl);

		if (validationResult.error) {
			setErrors(validationResult.error.details);
			return;
		}

		onAdd(imageUrl);
	};

	let body = <>
		<form onSubmit={handleSubmit}>
			<div className="space-y-4 mt-6">
				<TextInput
					name="imageUrl"
					placeholder="Image URL"
					position="alone"
					autoComplete="off"
					icon={PhotographIcon}
				/>

				<ErrorList errors={errors}/>

				<SubmitButton
					text="Add Image"
				/>
			</div>
		</form>
	</>;

	return (
		<Modal
			isOpen={isOpen}
			onClose={onClose}
			title="Add Image"
			body={body}
		/>
	);
};

export default AddImageModal;
