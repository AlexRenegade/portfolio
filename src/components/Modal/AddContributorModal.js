import React, {useState} from "react";
import Modal from "./Modal";
import {LinkIcon, PencilAltIcon} from "@heroicons/react/solid";
import TextInput from "../FormElements/TextInput";
import SubmitButton from "../FormElements/SubmitButton";
import ErrorList from "../Error/ErrorList";
import ContributorsSchema from "../../schemas/Frontend/ContributorsSchema";

const AddContributorModal = ({isOpen, onClose, onAdd}) => {
	const [errors, setErrors] = useState([]);

	const handleSubmit = (event) => {
		event.preventDefault();
		event.stopPropagation();

		let {
			name: nameElement,
			link: linkElement
		} = event.target;

		let name = nameElement.value;
		let link = linkElement.value;

		let validationResult = ContributorsSchema.validate({
			name,
			link
		}, {
			abortEarly: false,
			stripUnknown: true
		});

		if (validationResult.error) {
			setErrors(validationResult.error.details);
			return;
		}

		({
			name,
			link
		} = validationResult.value);

		onAdd({
			name,
			link
		});
	};

	let body = <>
		<form onSubmit={handleSubmit}>
			<div className="space-y-4 mt-6">
				<TextInput
					name="name"
					placeholder="Name"
					position="alone"
					autoComplete="off"
					icon={PencilAltIcon}
				/>

				<TextInput
					name="link"
					placeholder="Link"
					position="alone"
					autoComplete="off"
					icon={LinkIcon}
				/>

				<ErrorList errors={errors}/>

				<SubmitButton
					text="Add Contributor"
				/>
			</div>
		</form>
	</>;

	return (
		<Modal
			isOpen={isOpen}
			onClose={onClose}
			title="Add Contributor"
			body={body}
		/>
	);
};

export default AddContributorModal;
