import React from "react";
import SpotifyArtist from "./SpotifyArtist";

const SpotifyArtistsList = ({artists}) => {
	return (
		<p className="text-gray-400 text-base sm:text-lg lg:text-base xl:text-lg font-medium">
			By {artists.map((artist, i) => {
				let ending;

				if (i === artists.length - 2)
					ending = " and ";
				else if (i === artists.length - 1)
					ending = "";
				else
					ending = ", ";

				return <span key={artist.name}>
					<SpotifyArtist key={artist.name} name={artist.name} url={artist.url}/>
					{ending}
				</span>;
			})}
		</p>
	);
};

export default SpotifyArtistsList;
