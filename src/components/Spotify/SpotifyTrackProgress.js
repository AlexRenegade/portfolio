import React from "react";
import {parseMilliseconds} from "../../helpers/general";

const SpotifyTrackProgress = ({progress, progressPercentage, duration}) => {
	return (
		<div className="space-y-2">
			<div className="bg-black rounded-full overflow-hidden">
				<div className="bg-spotify h-1.5 rounded-full" role="progressbar"
					aria-valuenow={progressPercentage}
					aria-valuemin="0" aria-valuemax="100"
					style={{width: `${progressPercentage}%`}}
				/>
			</div>
			<div className="text-gray-400 flex justify-between text-sm font-medium tabular-nums">
				<div>{parseMilliseconds(progress)}</div>
				<div>{parseMilliseconds(duration)}</div>
			</div>
		</div>
	);
};

export default SpotifyTrackProgress;
