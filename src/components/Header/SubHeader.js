import React from "react";

const SubHeader = ({title, subtitle, width = "3xl"}) => {
	return (
		<div className="relative overflow-hidden">
			<div className="relative max-w-7xl ml-4 md:ml-8 lg:ml-16 px-2 sm:px-4
				lg:px-6 sm:static pt-8 lg:pt-0">
				<div className={`sm:max-w-${width}`}>
					<h1 className="text-3xl font font-extrabold tracking-tight text-white">
						{title}
					</h1>
					<p className="mt-2 ml-4 text-lg text-gray-100 font-bold">
						{subtitle}
					</p>
				</div>
			</div>
		</div>
	);
};

export default SubHeader;
