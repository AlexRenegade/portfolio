import React from "react";

const NumberInput = ({
	labelClassName,
	name,
	placeholder,
	position = "middle",
	disabled,
	onInput,
	defaultValue,
	icon: Icon,
	min = 0,
	max
}) => {
	let borderClass = "";

	if (position === "first")
		borderClass = "rounded-t-md";
	else if (position === "last")
		borderClass = "rounded-b-md";
	else if (position === "alone")
		borderClass = "rounded-t-md rounded-b-md";

	return (
		<>
			<label className={`relative focus-within:text-indigo-500
				focus-within:z-10 block ${labelClassName}`}>

				{Icon ?
					<Icon className="pointer-events-none w-8 h-7 absolute
					top-1/2 transform -translate-y-1/2 left-3"/> : ""
				}

				<input
					name={name}
					type="number"
					className={`form-input appearance-none rounded-none block w-full px-4
					py-3 pl-14 bg-dark-650 border border-dark-500 placeholder-gray-200 text-white
					focus:outline-none focus:ring-indigo-500 focus:border-indigo-500
					sm:text-sm autofill:border-red-900
					disabled:opacity-60 ` + borderClass}
					placeholder={placeholder}
					disabled={disabled}
					onInput={onInput}
					defaultValue={defaultValue}
					min={min}
					max={max}
				/>
			</label>
		</>
	);
};

export default NumberInput;
