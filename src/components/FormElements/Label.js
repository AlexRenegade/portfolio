import React from "react";

const Label = ({children, textAlign = "left"}) => {
	return (
		<>
			<p className={`text-${textAlign} font-bold mb-2 ml-2`}>
				{children}
			</p>
		</>
	);
};

export default Label;
