import React from "react";

const Checkbox = (
	{
		text,
		onChecked,
		name,
		colour,
		checked,
		disabled,
		readOnly,
		size = 5
	}
) => {
	return (
		<div className={`flex items-center ${disabled && !readOnly ? "opacity-50": ""}`}>
			<input
				onChange={onChecked}
				name={name}
				type="checkbox"
				defaultChecked={checked}
				className={`h-${size} w-${size} ml-2 text-${colour}-600 focus:ring-${colour}-500
					border-dark-300 rounded bg-dark-600`}
				disabled={disabled}
			/>
			<label className="ml-2 block text-sm text-white">
				{text}
			</label>
		</div>
	);
};

export default Checkbox;
