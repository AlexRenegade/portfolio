import React, {useEffect, useRef, useState} from "react";
import {DateTime} from "luxon";
import ListBox from "../ListBox/ListBox";
import {months, daysInMonths} from "../../lib/constants";

const monthOptions = months.map((month, index) => {
	return {
		text: month,
		value: index + 1
	};
});
const yearOptions = Array(131).fill().map((val, i) => {
	return {
		text: i + 1970,
		value: i + 1970
	};
});

const DateInput = ({text, onChange, disabled}) => {
	const selectedDate = useRef(1);
	const selectedMonth = useRef(1);
	const selectedYear = useRef(2021);

	const [dateOptions, setDateOptions] = useState([]);
	const [maxDate, setMaxDate] = useState(1);

	const getTimestamp = (dateObject) => {
		return DateTime.fromObject({
			day: dateObject.date,
			month: dateObject.month,
			year: dateObject.year
		}).toMillis();
	};

	const handleDateChanged = (newValue) => {
		selectedDate.current = newValue.value;

		let newSelectedDateObject = {
			date: selectedDate.current,
			month: selectedMonth.current,
			year: selectedYear.current
		};

		newSelectedDateObject.timestamp = getTimestamp(newSelectedDateObject);

		onChange(newSelectedDateObject);
	};

	const handleMonthChanged = (newValue) => {
		let {value} = newValue;
		selectedMonth.current = value;

		let daysInNewMonth = daysInMonths[selectedMonth.current - 1];

		if (daysInNewMonth < selectedDate.current)
			selectedDate.current = daysInNewMonth;
		else
			selectedMonth.current = newValue.value;

		let newSelectedDateObject = {
			date: selectedDate.current,
			month: selectedMonth.current,
			year: selectedYear.current
		};

		newSelectedDateObject.timestamp = getTimestamp(newSelectedDateObject);

		onChange(newSelectedDateObject);

		setDateOptions(Array(daysInNewMonth).fill().map((val, i) => {
			return {
				text: i + 1,
				value: i + 1
			};
		}));
		setMaxDate(daysInNewMonth);
	};

	const handleYearChanged = (newValue) => {
		selectedYear.current = newValue.value;

		let newSelectedDateObject = {
			date: selectedDate.current,
			month: selectedMonth.current,
			year: selectedYear.current
		};

		newSelectedDateObject.timestamp = getTimestamp(newSelectedDateObject);

		onChange(newSelectedDateObject);
	};

	useEffect(() => {
		if (dateOptions.length === 0)
			handleMonthChanged({value: 1});
	}, []);

	return (
		<>
			<h1 className={`text-xl font font-extrabold tracking-tight text-left ml-2 text-white
				${disabled ? "opacity-50" : ""}`}>
				{text}
			</h1>
			<div className="grid grid-cols-3 space-x-2">
				<ListBox
					key={maxDate}
					options={dateOptions}
					defaultValue={{
						text: selectedDate.current,
						value: selectedDate.current
					}}
					onChange={handleDateChanged}
					disabled={disabled}
				/>

				<ListBox
					options={monthOptions}
					defaultValue={monthOptions[0]}
					onChange={handleMonthChanged}
					disabled={disabled}
				/>

				{/* Index 51 is the year 2021 */}
				<ListBox
					options={yearOptions}
					defaultValue={yearOptions[51]}
					onChange={handleYearChanged}
					disabled={disabled}
				/>
			</div>
		</>
	);
};

export default DateInput;
