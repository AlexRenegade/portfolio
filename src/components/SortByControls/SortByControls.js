import React from "react";
import ListBox from "../ListBox/ListBox";
import {projectSortByOptions, sortByDirectionOptions} from "../../lib/constants";

const SortByControls = ({onSortChange, onDirectionChange}) => {
	return (
		<div className="grid grid-cols-1 bg-dark-650 rounded-lg my-4 px-4
			py-3 mx-9 lg:mx-33 sm:grid-cols-2 xl:grid-cols-4">
			<ListBox
				prefix="Sort By"
				options={projectSortByOptions}
				defaultValue={projectSortByOptions[0]}
				onChange={onSortChange}
			/>

			<ListBox
				prefix="Sort Direction"
				options={sortByDirectionOptions}
				defaultValue={sortByDirectionOptions[0]}
				onChange={onDirectionChange}
			/>
		</div>
	);
};

export default SortByControls;
