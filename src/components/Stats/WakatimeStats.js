import React from "react";
import useEndpoint from "../../lib/useEndpoint";
import SubHeader from "../Header/SubHeader";
import WakatimeGraph from "../Graph/Wakatime/WakatimeGraph";
import BulletList from "../BulletList/BulletList";
import BulletListItem from "../BulletList/BulletListItem";
import {DateTime, Duration} from "luxon";
import HighlightedStatistic from "./HighlightedStatistic";
import {pluralise, ordinal} from "../../helpers/general";
import Skeleton from "react-loading-skeleton";

const WakatimeStats = () => {
	const {stats: wakatimeStats, loading: wakatimeLoading} = useEndpoint("/api/wakatime", "stats",
		true, false);
	const {data: activityData, loading: wakatimeActivityLoading} =
		useEndpoint("/api/wakatime/lastActivity", "data",
			true, false);

	if (wakatimeLoading) {
		return (
			<>
				<SubHeader
					title="WakaTime Stats"
					subtitle="WakaTime is a tool for tracking the time spent developing
					within a range of IDEs and other software"
				/>

				<WakatimeGraph loading={true}/>

				<div className="px-8 gap-4 mb-6 lg:px-32">
					<BulletList className="mt-4">
						{Array(5).fill().map((val, index) => {
							return <BulletListItem key={index}>
								<Skeleton width="60%" height="1.5rem"/>
							</BulletListItem>;
						})}
					</BulletList>
				</div>
			</>
		);
	}

	let graphData = wakatimeStats.month.map(day => {
		return {
			name: day.date,
			Hours: (day.seconds / 3600)
		};
	});

	let bestDay = DateTime.fromFormat(wakatimeStats.bestDay.date, "yyyy-MM-dd");
	let daysSinceBestDay = Math.floor(Math.abs(bestDay.diffNow(["days"])
		.toObject()
		.days));

	let totalHours = graphData.reduce((a, b) => a + b.Hours, 0);
	let dailyAverage = totalHours / graphData.length;
	let parsedDailyAverage = Duration.fromObject({
		hours: dailyAverage
	}).shiftTo("hours", "minutes")
		.toObject();

	parsedDailyAverage.minutes = Math.floor(parsedDailyAverage.minutes);

	let dailyAverageHours =
		`${parsedDailyAverage.hours} ${pluralise(parsedDailyAverage.hours, "hr")}`;
	let dailyAverageMinutes =
		`${parsedDailyAverage.minutes} ${pluralise(parsedDailyAverage.minutes, "min")}`;
	let dailyAverageString = `${dailyAverageHours} ${dailyAverageMinutes}`;

	return (
		<>
			<SubHeader
				title="WakaTime Stats"
				subtitle="WakaTime is a tool for tracking the time spent developing
					within a range of IDEs and other software"
			/>

			<WakatimeGraph data={graphData} dailyAverage={dailyAverage}/>

			<div className="px-8 gap-4 mb-6 lg:px-32">
				<BulletList className="mt-4">
					<BulletListItem>
						My best day was
						<HighlightedStatistic padText={true}>
							{daysSinceBestDay} days ago
						</HighlightedStatistic>
						where I coded for
						<HighlightedStatistic padText={true}>
							{wakatimeStats.bestDay.text}
						</HighlightedStatistic>
					</BulletListItem>

					<BulletListItem>
						My daily average over the last 30 days is
						<HighlightedStatistic padText={true}>
							{dailyAverageString}
						</HighlightedStatistic>
					</BulletListItem>

					<BulletListItem>
						I have coded for
						<HighlightedStatistic padText={true}>
							{wakatimeStats.lastWeekTotal}
						</HighlightedStatistic>
						in the last week
					</BulletListItem>

					<BulletListItem>
						I am currently
						<HighlightedStatistic padText={true}>
							{ordinal(wakatimeStats.leaderboard.rank)}
						</HighlightedStatistic>
						in the global WakaTime leaderboard
					</BulletListItem>

					{wakatimeActivityLoading ?
						<BulletListItem>
							<Skeleton width="60%" height="1.5rem"/>
						</BulletListItem> :
						<BulletListItem>
							My last recorded activity was
							<HighlightedStatistic padText={true}>
								{DateTime.fromMillis(activityData.timestamp).toRelative()}
							</HighlightedStatistic>
							within
							<HighlightedStatistic padText={true}>
								{activityData.editor}
							</HighlightedStatistic>
						</BulletListItem>
					}
				</BulletList>
			</div>
		</>
	);
};

export default WakatimeStats;
