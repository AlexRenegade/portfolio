import React from "react";
import useEndpoint from "../../lib/useEndpoint";
import SubHeader from "../Header/SubHeader";
import BulletList from "../BulletList/BulletList";
import BulletListItem from "../BulletList/BulletListItem";
import Skeleton from "react-loading-skeleton";
import {DateTime} from "luxon";
import HighlightedStatistic from "./HighlightedStatistic";

const GithubStats = () => {
	const {data: githubData, loading: githubLoading} = useEndpoint("/api/github", "data",
		true, false);

	if (githubLoading) {
		return (
			<>
				<SubHeader
					title="GitHub Stats"
				/>

				<div className="px-8 gap-4 mb-6 lg:px-32">
					<BulletList className="mt-4">
						{Array(4).fill().map((val, index) => {
							return <BulletListItem key={index}>
								<Skeleton width="60%" height="1.5rem"/>
							</BulletListItem>;
						})}
					</BulletList>
				</div>
			</>
		);
	}

	let bestDay = DateTime.fromFormat(githubData.highestDay.date, "yyyy-MM-dd");
	let daysSinceBestDay = Math.floor(Math.abs(bestDay.diffNow(["days"])
		.toObject()
		.days));

	return (
		<>
			<SubHeader
				title="GitHub Stats"
				subtitle="I very rarely use GitHub, so these stats are likely to be extremely underwhelming"
			/>

			<div className="px-8 gap-4 mb-8 lg:px-32">
				<BulletList className="mt-4">
					<BulletListItem>
						I have made
						<HighlightedStatistic padText={true}>
							{githubData.contributionStreak.contributions.toLocaleString()}
						</HighlightedStatistic>
						contributions over the last
						<HighlightedStatistic padText={true}>
							{githubData.contributionStreak.days}
						</HighlightedStatistic>
						consecutive days.
					</BulletListItem>

					<BulletListItem>
						I have made
						<HighlightedStatistic padText={true}>
							{githubData.totalContributions.toLocaleString()}
						</HighlightedStatistic>
						contributions in the last year.
					</BulletListItem>

					<BulletListItem>
						My best day was
						<HighlightedStatistic padText={true}>
							{daysSinceBestDay} days ago
						</HighlightedStatistic>
						where I made
						<HighlightedStatistic padText={true}>
							{githubData.highestDay.count}
						</HighlightedStatistic>
						contributions
					</BulletListItem>

					<BulletListItem>
						My last contribution was
						<HighlightedStatistic padText={true}>
							{githubData.lastContributionTimestamp ?
								DateTime.fromMillis(githubData.lastContributionTimestamp).toRelative() :
								"90+ days ago"
							}
						</HighlightedStatistic>
					</BulletListItem>
				</BulletList>
			</div>
		</>
	);
};

export default GithubStats;
