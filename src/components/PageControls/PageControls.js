import React from "react";
import PageNumberButton from "./PageNumberButton";
import PageMoveLeftButton from "./PageMoveButton/PageMoveLeftButton";
import PageMoveRightButton from "./PageMoveButton/PageMoveRightButton";
import MobilePageMoveRightButton from "./PageMoveButton/MobilePageMoveRightButton";
import MobilePageMoveLeftButton from "./PageMoveButton/MobilePageMoveLeftButton";

const PageControls = ({currentPage, totalPages, onPageChange, totalResults, perPage}) => {
	let pageNumberControls;

	const handlePageNumberButtonClicked = (pageNumber) => {
		onPageChange(pageNumber);
	};

	const handlePageLeftClicked = () => {
		onPageChange(currentPage - 1);
	};

	const handlePageRightClicked = () => {
		onPageChange(currentPage + 1);
	};

	if (totalPages <= 6) {
		pageNumberControls = <>
			<PageMoveLeftButton onClick={handlePageLeftClicked} disabled={currentPage === 1}/>
			{Array(totalPages).fill().map((value, i) => {
				return <PageNumberButton
					key={i}
					pageNumber={i + 1}
					active={i + 1 === currentPage}
					onClick={handlePageNumberButtonClicked}
				/>;
			})}
			<PageMoveRightButton onClick={handlePageRightClicked} disabled={currentPage === totalPages}/>
		</>;
	} else if (currentPage === 1 || currentPage === 2 || currentPage === 3) {
		pageNumberControls = <>
			<PageMoveLeftButton onClick={handlePageLeftClicked} disabled={currentPage === 1}/>
			<PageNumberButton
				pageNumber={1} active={currentPage === 1}
				onClick={handlePageNumberButtonClicked}/>
			<PageNumberButton
				pageNumber={2} active={currentPage === 2}
				onClick={handlePageNumberButtonClicked}/>
			<PageNumberButton
				pageNumber={3} active={currentPage === 3}
				onClick={handlePageNumberButtonClicked}/>
			<PageNumberButton
				pageNumber="..." disabled={true}
				onClick={handlePageNumberButtonClicked}/>
			<PageNumberButton
				pageNumber={totalPages} onClick={handlePageNumberButtonClicked}/>
			<PageMoveRightButton
				onClick={handlePageRightClicked} disabled={currentPage === totalPages}/>
		</>;
	} else if (currentPage === totalPages || currentPage === totalPages - 1 ||
		currentPage === totalPages - 2) {
		pageNumberControls = <>
			<PageMoveLeftButton onClick={handlePageLeftClicked} disabled={currentPage === 1}/>
			<PageNumberButton
				pageNumber={1} onClick={handlePageNumberButtonClicked}/>
			<PageNumberButton
				pageNumber="..." disabled={true}
				onClick={handlePageNumberButtonClicked}/>
			<PageNumberButton
				pageNumber={totalPages - 2} active={currentPage === totalPages - 2}
				onClick={handlePageNumberButtonClicked}/>
			<PageNumberButton
				pageNumber={totalPages - 1} active={currentPage === totalPages - 1}
				onClick={handlePageNumberButtonClicked}/>
			<PageNumberButton
				pageNumber={totalPages} active={currentPage === totalPages}
				onClick={handlePageNumberButtonClicked}/>
			<PageMoveRightButton
				onClick={handlePageRightClicked} disabled={currentPage === totalPages}/>
		</>;
	} else {
		pageNumberControls = <>
			<PageMoveLeftButton onClick={handlePageLeftClicked} disabled={currentPage === 1}/>
			<PageNumberButton
				pageNumber={1}
				onClick={handlePageNumberButtonClicked}/>
			<PageNumberButton
				pageNumber="..." disabled={true}
				onClick={handlePageNumberButtonClicked}/>
			<PageNumberButton
				pageNumber={currentPage} active={true}
				onClick={handlePageNumberButtonClicked}/>
			<PageNumberButton
				pageNumber="..." disabled={true}
				onClick={handlePageNumberButtonClicked}/>
			<PageNumberButton
				pageNumber={totalPages} onClick={handlePageNumberButtonClicked}/>
			<PageMoveRightButton
				onClick={handlePageRightClicked} disabled={currentPage === totalPages}/>
		</>;
	}

	return (
		<div className="bg-dark-650 px-4 py-3 flex items-center
			justify-between mx-9 lg:mx-33 rounded-lg my-4">
			<div className="flex-1 flex justify-between sm:hidden">
				<MobilePageMoveLeftButton
					disabled={currentPage === 1} onClick={handlePageLeftClicked}/>
				<MobilePageMoveRightButton
					disabled={currentPage === totalPages} onClick={handlePageRightClicked}/>
			</div>
			<div className="hidden sm:flex-1 sm:flex sm:items-center sm:justify-between">
				<div>
					<p className="text-sm">
						Showing
						<span className="font-medium mx-1">
							{totalResults === 0 ? 0 : (currentPage - 1) * perPage + 1}
						</span>
						to
						<span className="font-medium mx-1">
							{currentPage * perPage < totalResults ? currentPage * perPage : totalResults}
						</span>
						of
						<span className="font-medium mx-1">{totalResults}</span>
						results
					</p>
				</div>
				<div>
					<div className="relative z-0 inline-flex rounded-md shadow-sm -space-x-px">
						{pageNumberControls}
					</div>
				</div>
			</div>
		</div>
	);
};

export default PageControls;
