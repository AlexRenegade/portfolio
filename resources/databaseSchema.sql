create table if not exists portfolio.contacts
(
    id           char(22)      not null,
    title        varchar(64)   not null,
    text         varchar(512)  not null,
    link         varchar(1024) null,
    baseSiteLink varchar(512)  null,
    icon         varchar(128)  null,
    iconColour   char(6)       null,
    constraint contact_id_uindex
    unique (id)
)
    charset = utf8mb4;

alter table portfolio.contacts
    add primary key (id);

create table if not exists portfolio.credentials
(
    id        char(22)    not null,
    type      varchar(32) not null,
    data      mediumtext  not null,
    expiresAt bigint      not null,
    constraint credentials_id_uindex
    unique (id)
)
    charset = utf8mb4;

create index credentials_type_index
    on portfolio.credentials (type);

alter table portfolio.credentials
    add primary key (id);

create table if not exists portfolio.education
(
    id        char(20)     not null,
    location  varchar(128) not null,
    level     varchar(64)  not null,
    subject   varchar(64)  not null,
    startDate smallint     not null,
    endDate   smallint     not null,
    grade     varchar(32)  not null,
    constraint education_id_uindex
    unique (id)
)
    charset = utf8mb4;

alter table portfolio.education
    add primary key (id);

create table if not exists portfolio.experience
(
    id          char(20)     not null,
    title       varchar(128) not null,
    description text         not null,
    company     varchar(128) not null,
    url         varchar(128) null,
    iconUrl     varchar(256) null,
    endDate     bigint       null,
    startDate   bigint       null,
    constraint education_id_uindex
    unique (id)
)
    charset = utf8mb4;

alter table portfolio.experience
    add primary key (id);

create table if not exists portfolio.projectSkills
(
    id        char(26) not null,
    projectId char(21) not null,
    skillId   char(22) not null,
    constraint projectSkills_id_uindex
    unique (id)
)
    charset = utf8mb4;

create index projectSkills_projects_id_fk
    on portfolio.projectSkills (projectId);

create index projectSkills_skills_id_fk
    on portfolio.projectSkills (skillId);

alter table portfolio.projectSkills
    add primary key (id);

create table if not exists portfolio.projects
(
    id               char(21)             not null
    primary key,
    name             varchar(128)         not null,
    shortDescription varchar(256)         null,
    longDescription  mediumtext           null,
    openSource       tinyint(1) default 0 not null,
    repository       varchar(1024)        null,
    contributors     mediumtext           null,
    imageUrl         varchar(512)         null,
    dateAdded        bigint               not null
)
    charset = utf8mb4;

create index projects_dateAdded_index
    on portfolio.projects (dateAdded);

create index projects_id_index
    on portfolio.projects (id);

create index projects_openSource_index
    on portfolio.projects (openSource);

create table if not exists portfolio.skills
(
    id       char(22)      not null,
    type     varchar(128)  not null,
    name     varchar(128)  not null,
    link     varchar(1024) null,
    imageUrl varchar(1024) not null,
    colour   char(6)       not null,
    constraint skills_id_uindex
    unique (id)
)
    charset = utf8mb4;

alter table portfolio.skills
    add primary key (id);

create table if not exists portfolio.users
(
    id                     char(21)             not null,
    username               varchar(128)         null,
    password               char(60)             not null,
    avatarUrl              varchar(256)         null,
    webauthnLoginEnabled   tinyint(1) default 0 not null,
    webauthnChallenge      char(44)             null,
    webauthnAuthrInfo      mediumtext           null,
    webauthnLoginChallenge char(44)             null,
    constraint users_id_uindex
    unique (id)
)
    charset = utf8mb4;

create index users_username_index
    on portfolio.users (username);

alter table portfolio.users
    add primary key (id);

